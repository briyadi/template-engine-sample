'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Articles', [
        {
            id: 1,
            title: "Express JS",
            body: "Fast, unopinionated, minimalist web framework for Node.js",
            created_at: new Date(),
            updated_at: new Date(),
        },
        {
            id: 2,
            title: "EJS",
            body: "Embedded JavaScript templating.",
            created_at: new Date(),
            updated_at: new Date(),
        },
        {
            id: 3,
            title: "Sequelize",
            body: "Sequelize is a modern TypeScript and Node.js ORM for Oracle, Postgres, MySQL, MariaDB, SQLite and SQL Server, and more. Featuring solid transaction support, relations, eager and lazy loading, read replication and more.",
            created_at: new Date(),
            updated_at: new Date(),
        }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Articles', {id: [1, 2, 3]}, {});
  }
};
