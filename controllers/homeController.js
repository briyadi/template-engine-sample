module.exports = {
    index: (req, res) => {
        let params = {
            title: "Welcome",
            articles: [
                "Express JS",
                "EJS",
                "Sequelize"
            ]
        }

        // views/index.ejs
        res.render("index", params) 
    }
}