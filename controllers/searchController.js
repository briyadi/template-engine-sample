const { Op } = require("sequelize")
const { Article } = require("../models")

module.exports = {
    index: async (req, res) => {
        let results = []

        if (req.query) {
            try {
                results = await Article.findAll({
                    where: {
                        body: {
                            [Op.iLike]: `%${req.query.q}%`
                        }
                    }
                })
            } catch (e) {
                console.log(e.message)
            }
        }

        let params = {
            title: "Search",
            articles: results,
        }

        res.render("search", params)
    }
}