const router = require("express").Router()

// .. definisi router
const home = require("./controllers/homeController")
const about = require("./controllers/aboutController")
const search = require("./controllers/searchController")

router.get("/", home.index)
router.get("/about", about.index)
router.get("/search", search.index)

module.exports = router